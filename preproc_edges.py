import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ["KERAS_BACKEND"]="tensorflow"
from lib_csscnet.file_utils import *
import numpy as np # linear algebra
import multiprocessing
import argparse
import time
import random



##################################
# Parameters
##################################
#Train SUNCG
#BASE_PATH = '/(...)/sscnet/data'
#DATASET = 'SUNCGtrain'


#Eval SUNCG
#BASE_PATH = '/(...)/sscnet/data/depthbin'
#DATASET = 'SUNCGtest_49700_49884'


#TrainNYU
#DATASET = 'NYUtrain'
#BASE_PATH = '/d02/data/csscnet/NYU'

#TestNYU
#DATASET = 'NYUtest'
#BASE_PATH = '/d02/data/csscnet/NYU'

#TrainSUNCGGen
DATASET = 'SUNCGGENtrain'
BASE_PATH = '/d02/data/csscnet/SUNCG'

#TestSUNCGGen
#DATASET = 'SUNCGGENtest'
#BASE_PATH = '/d02/data/csscnet/SUNCG'


DEST_PATH = '/d02/data/csscnet_edges_preproc'

DEVICE = 1

THREADS = 4

# Globals
proc_prefixes = multiprocessing.Value('i',0)
total_prefixes = 0
processed = 0


##################################

def parse_arguments():
    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS

    print("\nEDGENET PREPROCESSOR\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Directory (related to base_path), of the original data", type=str)
    parser.add_argument("--base_path", help="Base path of the original data. Default: "+BASE_PATH, type=str, default=BASE_PATH, required=False)
    parser.add_argument("--dest_path", help="Destination path. Default: "+DEST_PATH, type=str, default=DEST_PATH, required=False)
    parser.add_argument("--device", help="CUDA device. Default " + str(DEVICE), type=int, default=DEVICE, required=False)
    parser.add_argument("--threads", help="Concurrent threads. Default " + str(THREADS), type=int, default=THREADS, required=False)
    args = parser.parse_args()

    BASE_PATH = args.base_path
    DATASET = args.dataset
    DEST_PATH = args.dest_path
    DEVICE = args.device
    THREADS = args.threads

def process_multi(file_prefix):

    worker = multiprocessing.current_process()._identity[0]

    if worker<=4:
        worker_device = 0
    else:
        worker_device = 1


    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS, to_process, proc_prefixes, total_time, processed

    if file_prefix not in  [
        "/d02/data/csscnet/SUNCG/Val/7724bf6205df58dde671dc1c84e11b80/00000015",
        "/d02/data/csscnet/SUNCG/Train/4185b8d8c608d88f61b04d6c0967c87a/00000028",
    ]:

        lib_sscnet_setup(device=worker_device, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)

        dest_prefix = DEST_PATH + file_prefix[len(BASE_PATH):]
        preproc_file = dest_prefix + '.npz'

        directory = os.path.split(dest_prefix)[0]

        with proc_prefixes.get_lock():
            if not os.path.exists(directory):
                os.makedirs(directory)

        shape = (240, 144, 240)

        #vox_tsdf, vox_rgb, segmentation_label, vox_weights = process(file_prefix, voxel_shape=shape, down_scale=4)
        vox_tsdf, vox_edges, tsdf_edges, segmentation_label, vox_weights, vox_vol = process(file_prefix,
                                                                                   voxel_shape=shape,
                                                                                   down_scale=4,
                                                                                   input_type=InputType.DEPTH_EDGES)

        down_shape=(shape[0]//4, shape[1]//4, shape[2]//4 )

        if np.sum(segmentation_label) == 0:
            print(file_prefix)
            #exit(-1)
        else:

            np.savez_compressed(preproc_file,
                           tsdf=vox_tsdf.reshape((shape[0], shape[1], shape[2], 1)),
                           edges=tsdf_edges.reshape((shape[0], shape[1], shape[2], 1)),
                           lbl=segmentation_label.reshape((down_shape[0], down_shape[1], down_shape[2], 1)),
                           weights= vox_weights.reshape((down_shape[0], down_shape[1], down_shape[2])),
                           vol= vox_vol.reshape((down_shape[0], down_shape[1], down_shape[2])))

    with proc_prefixes.get_lock():
        proc_prefixes.value += 1

        counter = proc_prefixes.value
        mean_time = (time.time() - total_time)/counter
        eta_time = mean_time * (to_process - counter)
        eta_h = eta_time // (60*60)
        eta_m = (eta_time - (eta_h*60*60))//60
        eta_s = eta_time - (eta_h*60*60) - eta_m * 60
        perc = 100 * counter/to_process

        print("  %3.2f%%  Mean Processing Time: %.2f seconds ETA: %02d:%02d:%02d     " % (perc, mean_time, eta_h, eta_m, eta_s), end="\r")


# Main Function
def Run():
    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS, to_process, proc_prefixes, total_time
    parse_arguments()

    print(" .")
    processed_files = get_file_prefixes_from_path(os.path.join(DEST_PATH, DATASET), criteria='*.npz')

    print(" ..")
    processed_files = [x[-41:] for x in processed_files]


    data_path = os.path.join(BASE_PATH, DATASET)

    print("Data path %s" % data_path)
    all_prefixes = get_file_prefixes_from_path(data_path)

    file_prefixes = [x for x in all_prefixes if x[-41:] not in processed_files]
    to_process = len(file_prefixes)

    print("Files: %d. Already processed: %d. To process: %d.\n" % (len(all_prefixes), len(processed_files), to_process))

    total_time = time.time()

    if to_process>0:
        pool = multiprocessing.Pool(processes=THREADS)

        pool.map(process_multi, file_prefixes)
        pool.close()
        pool.join()

    print("Total time: %s seconds                                                       " % (time.time() - total_time))

if __name__ == '__main__':
  Run()