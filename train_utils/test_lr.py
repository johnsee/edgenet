def one_cycle_lr(iteration):
    """Given the inputs, calculates the lr that should be applicable for this iteration"""
    point1 = 10
    point2 = 20
    point3 = 25
    base_lr = 0.01
    max_lr = 0.1
    min_lr = 0.00001
    if iteration < point1:
        lr = base_lr + iteration*(max_lr-base_lr)/point1
    elif iteration < point2:
        lr = max_lr - (iteration-point1)*(max_lr-base_lr)/(point2-point1)
    elif iteration < point3:
        lr = base_lr - (iteration-point2)*(base_lr-min_lr)/(point3-point2)
    else:
        lr = min_lr

    return lr

for i in range(30):
    print(i,one_cycle_lr(i))


